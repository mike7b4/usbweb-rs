use std::io;
use serde_json::json;
use toml::toml;
use usbapi::*; 
use std::env;
use std::fs::File;
use std::io::prelude::*;

use futures::{future, Future, Stream};

use actix::System;

use actix_web::http::{header, Method, StatusCode};
use actix_web::middleware::session::{RequestSession};
use actix_web::{
    error, fs, middleware, pred, server, App, Error, HttpRequest, HttpResponse, Path,
    Result,
};
static NOTFOUND: &[u8] = b"Not Found";
fn p404(_req: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::NOT_FOUND).body("404"))
}

fn response_wwan(_req: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::NOT_FOUND).body("TBD"))
}

fn response_wlan(_req: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::NOT_FOUND).body("TBD"))
}

fn response_links(_req: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::NOT_FOUND).body("TBD"))
}

fn response_index(_req: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::NOT_FOUND).body("Index page"))
}

fn response_system(_req: &HttpRequest) -> Result<HttpResponse> {
    // Ugly hackish I should probably lookup all thermal zones not only the first
    let res: Result<String, Error> = {
        let mut sysfile = File::open("/sys/class/thermal/thermal_zone0/temp")?;
        let mut value = String::new();
        sysfile.read_to_string(&mut value)?;
        /// FIXME
        /// WTF! I just want to get rid of the '\n'
        /// There must be easier way doing this
        /// ???
        let temperature : Vec<&str> = value.split("\n").collect();
        let temperature = temperature.iter().next().unwrap_or(&"");
        Ok(String::from(*temperature))
    };
    match res {
        Ok(temperature) => {
            let mut js = json!({});
            js["temperature"] = json!(temperature);
            Ok(HttpResponse::build(StatusCode::OK)
                                   .content_type("text/json")
                                   .body(js.to_string()))
        },
        Err(_) => {
            Ok(HttpResponse::build(StatusCode::NOT_FOUND).body("EPIC FAIL"))
        }
    }
}

fn response_enumerate(_req: &HttpRequest) -> Result<HttpResponse> {
    let mut usb = UsbEnumerate::new();
    let e = usb.enumerate();
    match e {
        Ok(_) => {
            Ok(HttpResponse::build(StatusCode::OK)
                                   .content_type("text/json")
                                   .body(json!(usb.devices()).to_string()))
        },
        Err(_) => {
            Ok(HttpResponse::build(StatusCode::NOT_FOUND).body("EPIC FAIL"))
        }
    }

}

fn main() {

    let mut ip = "localhost:8080".to_string();
    if env::args().len() > 1 {
        ip = env::args().nth(1).unwrap_or("localhost:8080".to_string());
    }
    let sys = actix::System::new("Usbweb");
    println!("Listening on {}", ip);
    let addr = server::new(
        || App::new()
            .resource("/", |r| r.method(Method::GET).f(response_index))
            .resource("/wwan", |r| r.method(Method::GET).f(response_wwan))
            .resource("/wlan", |r| r.method(Method::GET).f(response_wlan))
            .resource("/network", |r| r.method(Method::GET).f(response_links))
            .resource("/system", |r| r.method(Method::GET).f(response_system))
            .resource("/usb", |r| r.method(Method::GET).f(response_enumerate))
            .default_resource(|r| r.method(Method::GET).f(p404)))
        .bind(ip).expect("Can not bind")
        .shutdown_timeout(0)
        .start();

    let _ = sys.run();
}
